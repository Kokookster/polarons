from polarons.polaron_BFGS import Polaron_BFGS
from ase.io import read
from ase.calculators.aims import Aims


fmax = 0.001
steps = 100
delta = 0.001
factor = 1.0e0
fd_approx = 2

# Setup the aims calculator
calc = Aims(
    xc="PBE",
    k_grid=(2, 2, 2),
    charge=0.0,
    elsi_restart="read_and_write 100",
    sc_accuracy_forces=1e-4,
    collect_eigenvectors=False,
    aims_command="srun '/u/seko/software/FHIaims/build/O3/aims.200422.scalapack.mpi.x'",
    outfilename="aims.out",
    species_dir="/u/seko/software/FHIaims/species_defaults/light/",
)

# Read the FHI-aims geometry file
c = read("geometry.in")
# Attach calculator
c.set_calculator(calc)

# Generates the Polaron_BFGS optimizer
opt = Polaron_BFGS(
    c, delta=delta, factor=factor, fd_approx=fd_approx, trajectory="geo.traj"
)

with open("steps.dat", "a") as f:
    # Start optimization: Iterate through relaxation steps and writes some information
    # to `steps.dat`.
    for ii, _ in enumerate(opt.irun(fmax=fmax, steps=steps)):
        epot = opt.atoms.get_potential_energy()
        ho = opt.atoms.get_holu_level()
        f.write("{:4d} {:12.5f} {:12.5f}\n".format(ii, ho, epot))
