from polarons.system_info import System_info
from ase.io import read
import os

def test_system_info():
    try:
        c = read('test/geometry-MgO.in')
        si = System_info(c,1e-2)
        with open('test/system_info.txt_test','r') as si_file:
            ref = si_file.read()
        assert ref == str(si)

    finally:
        pass
        # os.remove('test/system_info.txt')
