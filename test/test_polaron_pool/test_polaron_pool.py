from polarons.polaron_pool import Polaron_pool
from ase.io import read
import os
from pathlib import Path
import shutil


def test_polaron_pool():
    prev_cwd = Path.cwd()
    os.chdir('test/test_polaron_pool/')

    c = read('geometry.in')
    try:
        Pp = Polaron_pool(c,[[1,0,0],[0,1,0],[0,0,1]],['S'],['Ba','Cu','Sn'],1e-2)


        with open('polarons.log_test','r') as reference:
            ref = reference.read()

        with open('polarons.log','r') as log:
            l = log.read()
    finally:
        shutil.rmtree('electron')
        shutil.rmtree('hole')
        os.remove('polarons.log')

    os.chdir(prev_cwd)

    assert l == ref
