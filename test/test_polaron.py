from polarons.polaron import Polaron
from ase.io import read
import os


def test_polaron():
    try:
        c = read('test/geometry-MgO.in')
        center = c[1].position
        P = Polaron(c,[[3,0,0],[0,3,0],[0,0,3]],['O'],['Mg'],center,'hole')
        P.write('test/geometry-MgO-polaron.in')
        cp = read('test/geometry-MgO-polaron.in')
        assert len(cp) == 216
    finally:
        os.remove('test/geometry-MgO-polaron.in')
