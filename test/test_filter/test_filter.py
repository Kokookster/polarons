from polarons.polaron_filter import Polaron_filter
from ase.io import read
from ase.calculators.aims import Aims


calc = Aims(
    xc="PBE",
    k_grid=(2, 2, 2),
    elsi_restart="read_and_write 100",
    sc_accuracy_forces=1e-4,
    collect_eigenvectors=False,
    aims_command="srun '/u/seko/software/FHIaims/build/O3/aims.200422.scalapack.mpi.x'",
    outfilename="aims.out",
    species_dir="/u/seko/software/FHIaims/species_defaults/light/",
)

def test_filter():
    c = read("test/test_filter/geometry.in")
    c.set_calculator(calc)
    pf = Polaron_filter(c)
    hasattr(pf,'atoms_list')

    assert hasattr(pf,'atoms_list')
    assert hasattr(pf,'fd_approx')
    assert hasattr(pf,'delta')
    assert hasattr(pf,'factor')
    assert hasattr(pf,'fd_operator')

# test_filter()
