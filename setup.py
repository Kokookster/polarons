from setuptools import setup, find_packages

setup(
    name="polarons",
    packages=find_packages(),
    install_requires = ['ase==3.19.1','spglib==1.15.0','pytest'],
    version='0.1.0'
)
