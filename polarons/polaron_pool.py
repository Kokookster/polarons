from polarons.system_info import System_info
from polarons.polaron import Polaron
from pathlib import Path


class Polaron_pool(System_info):
    """Generates a pool of atom-centered polaron geometries"""
    def __init__(self,unit_cell, supercell_matrix, anions, cations, sym_thresh=1e-5):
        System_info.__init__(self, unit_cell, sym_thresh)
        self.unit_cell = unit_cell
        self.anions = anions
        self.cations = cations
        self.hole_polarons = {}
        self.electron_polarons = {}
        self.supercell_matrix = supercell_matrix

        self.add_info('anions',anions,'List of anion element symbols')
        self.add_info('cations',cations,'List of cation element symbols')
        self.add_info('supcell_matrix',supercell_matrix,'Supercell Matrix')

        for anion in anions:
            self.hole_polarons[anion] = []
        for cation in cations:
            self.electron_polarons[cation] = []

        for uea in self.unique_equivalent_atoms:
            symbol = unit_cell[uea].symbol
            if symbol in anions:
                self.hole_polarons[symbol].append(uea)
            elif unit_cell[uea].symbol in cations:
                self.electron_polarons[symbol].append(uea)

        print(self.hole_polarons)
        print(self.electron_polarons)

        self.generate_polarons(self.hole_polarons,'hole')
        self.generate_polarons(self.electron_polarons,'electron')

        with open('polarons.log','w') as info_file:
            info_file.write(str(self))


    def generate_polarons(self,polarons_dict,ptype):
        p_list = []
        for species, polarons in polarons_dict.items():
            for center_ind in polarons:
                p = Path(ptype) / f'{center_ind}_{species}'
                p.mkdir(parents=True)
                center = self.unit_cell[center_ind].position
                polaron = Polaron(
                    self.unit_cell, self.supercell_matrix, self.anions, self.cations, center, ptype
                )
                p = p / 'geometry.in'
                polaron.write(p.as_posix())
                p_list.append(f'{center_ind}_{species}')

        self.add_info(f'{ptype}_polarons',p_list,f'{ptype} polaron')
