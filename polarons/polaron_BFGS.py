from ase.optimize import BFGS
from polarons.polaron_filter import Polaron_filter
from shutil import move

class Polaron_BFGS(BFGS):
    """
    Wrapper around the actual ASE BFGS algorithm with `Polaron_filter` applied.

    Does some file organization in addtion, that is, renaming the output file after
    each step.

    # TODO: Organize the restart.
    """

    def __init__(
        self, atoms, delta=0.001, factor=1.0, fd_approx=2, **kwargs
    ):

        atoms_pf = Polaron_filter(atoms, delta=0.001, factor=1.0, fd_approx=2)

        BFGS.__init__(self, atoms_pf,**kwargs)

        self.attach(self.callback_update_outfilename, interval=1)


    def callback_update_outfilename(self):
        step = self.nsteps
        try:
            for a in self.atoms.atoms_list:
                directory = a.calc.directory
                outfilename = directory + '/' + a.calc.outfilename
                move(outfilename,'{}_{}'.format(outfilename,step))
        except:
            pass
