from ase.atoms import Atoms
import spglib as spg
import numpy as np

class System_info():


    def __init__(self,system, sym_thresh=1e-5):

        self.info = {}
        self.atoms = system
        self.add_info('n_atoms', len(system),"Number of atoms")
        self.add_info('formula',system.get_chemical_formula(),"Chemical formula")

        if all(system.pbc):
            lattice = system.get_cell()[:]
            positions = system.get_scaled_positions()
            numbers = system.get_atomic_numbers()
            magmoms = system.get_initial_magnetic_moments()
            spg_cell = (lattice, positions, numbers, magmoms)
            dataset = spg.get_symmetry_dataset(spg_cell, symprec=sym_thresh)
            plattice, pscaled_positions, pnumbers = spg.find_primitive(
                spg_cell, symprec=sym_thresh
            )
            self.primitive = Atoms(
                cell=plattice,scaled_positions=pscaled_positions,numbers=pnumbers
            )
            self.add_info('sym_thresh',sym_thresh,'Symmetry Threshold')
            bravais = system.cell.get_bravais_lattice(eps=sym_thresh)
            self.add_info(
                'bravais',
                "{} {}".format(bravais.longname, bravais),
                "Bravais Lattice"
            )
            self.add_info(
                'unit_cell_parameters',
                np.around(system.get_cell_lengths_and_angles(), 12),
                "Unit cell parameters"
            )
            self.add_info('spacegroup', dataset["number"], "Spacegroup number")
            self.add_info('hall_symbol', dataset["hall"],"Hall symbol")
            self.add_info(
                'occupied_wyckoffs',
                np.unique(dataset["wyckoffs"]),
                "Occupied Wyckoff positions"
            )
            self.equivalent_atoms = dataset['equivalent_atoms']
            self.unique_equivalent_atoms = np.unique(dataset['equivalent_atoms'])
            self.add_info(
                'equivalent_atoms',
                np.unique(dataset['equivalent_atoms']),
                'Unique equivalent atoms'
            )
            self.add_info('is_primitive', len(system) == len(pnumbers),"Is primitive cell?")



    def add_info(self,key,value,info_str=''):
        self.info[key] = {
            'value': value,
            'info_str': info_str
        }

    def __str__(self):
        str_str = "System Info\n" + "-" * 14 + "\n"
        for key, idict in self.info.items():
            # print(key,idict)
            if isinstance(idict['value'], (list, np.ndarray)):
                fmt_str = "{:30}: " + "{} " * len(idict['value']) + "\n"
                str_str += fmt_str.format(idict['info_str'], *idict['value'])
            else:
                fmt_str = "{:30}: {}\n"
                str_str += fmt_str.format(idict['info_str'], idict['value'])
        return str_str
