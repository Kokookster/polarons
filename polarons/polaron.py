import numpy as np
from ase.build.supercells import make_supercell
from ase.io import write
# from ase.visualize import view

class Polaron:

    def __init__(self, unit_cell, supercell_matrix, anions, cations, center, ptype, sigma=None, d_max=0.2):
        """
        Generates a supercell from `unit_cell` with initial discplacements for a
        polaron calculation.

        Parameters:

        unit_cell: ASE atoms object
            unit cell to generate the supercell from
        supercell_matrix: 3x3 matrix
            supercell matrix
        anions: list of strings
            Lists the element symbols which are to be considered as anions
        cations: list of strings
            Lists the element symbols which are to be considered as cations
        center: float array
            Cartesian coordinates of the polaron center.
        ptype: string
            Polaron type: either `electron` or `hole`.
        sigma: float
            Width of the initial Gaussian distribution of ions. If `None` the width is
            chosen to 3*sigma = half of the cell diagonal
        d_max: float
            Maximum displacement of the ions.
        """

        self.center = center
        self.ptype = ptype

        sm = np.asarray(supercell_matrix)
        if sm.shape == (3,3):
            self.supercell = make_supercell(unit_cell, sm)
        else:
            print('ERROR: SUPERCELL_MATRIX has wrong shape. Should be 3x3.')
            exit()

        if not sigma:
            cell = self.supercell.get_cell()
            self.sigma = np.linalg.norm(np.sum(cell,axis=0)*0.5)/3.
        self.d_max = d_max

        self.anion_mask = self.get_ions_mask(anions)
        self.cation_mask = self.get_ions_mask(cations)
        self.generate_displacements()


    def generate_displacements(self):

        cell = self.supercell.get_cell()
        supercell = self.supercell

        # Construct Central Cell (center at origin)
        center_sc = supercell.copy()
        shift_vector = np.sum(cell,axis=0)*0.5 - self.center
        center_sc.positions += shift_vector
        center_sc.positions = center_sc.get_positions(wrap=True) - np.sum(cell,axis=0)*0.5

        # Create Displacements
        dist_vector = -center_sc.get_positions()
        dist = np.linalg.norm(dist_vector,axis=1)
        disp = gaussian(dist,0,self.sigma)*self.d_max

        # Move anion/cation away from electron/hole, respectively.
        if self.ptype == 'electron':
            disp[self.anion_mask] *= -1
        elif self.ptype == 'hole':
            disp[self.cation_mask] *= -1
        dist_vector = [
            dp*v/d if d > 1e-2 else v*0. for v,d,dp in zip(dist_vector,dist,disp)
        ]

        # Add distortions and rattle the atoms a little
        self.supercell.positions += dist_vector
        self.supercell.rattle()


    def get_ions_mask(self, ions):
        ion_mask = []
        for atom in self.supercell:
            ion_mask.append(atom.symbol in ions)

        return ion_mask

    def write(self,filename='geometry.in',format='aims'):
        write(filename,self.supercell,format)


def gaussian(x, mu, sigma):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sigma, 2.)))
