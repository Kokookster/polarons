from ase.constraints import Filter
import numpy as np
from copy import deepcopy


class Polaron_filter(Filter):
    """
    Organizes the polaron DFT calculation accroding to Sadigh et al.

    The Polaron_filter calss extends the ASE Filter class. Applying this filter on a
    ASE atoms object generates `fd_approx+1` atom objects and caclulators.

    Parameters:

    atoms: ASE atoms object
        Has to be an ASE atoms object with attached calculator
    delta: float
        The charge fraction for the finite difference approximation to calculate the
        homo/lumo force.
        Default value: 0.001
    factor: float
        Optional enhancement factor in the finite difference approximation to speed up
        optimization convergence. At the end this factor SHOULD BE 1.0 for any
        meaningful result.
        Default value: 1.0
    fd_approx: integer
        Degree of the finite difference approximation to calculate the force of the
        homo/lumo level. Currently only first (`fd_approx=1`) and second order
        (`fd_approx=2`) implemented
        Default value: 2
    """

    def __init__(self, atoms, delta=0.001, factor=1.0, fd_approx=2):

        mask = np.ones(len(atoms), dtype=bool)

        Filter.__init__(self, atoms, mask=mask)

        self.fd_approx = fd_approx
        self.delta = delta
        self.factor = factor
        self.atoms_list = []
        self.fd_operator = Fd_operator(fd_approx, factor, delta)

        calc = atoms.get_calculator()
        init_charge = 0.0
        try:
            if 'charge' in calc.parameters:
                init_charge = calc.parameters['charge']
        except:
            print("You should attach a calculator before creating the Filter")
            raise

        for i in range(fd_approx + 1):
            a = atoms.copy()
            calc_tmp = deepcopy(calc)
            charge = init_charge+delta * i
            calc_tmp.set(charge=charge)
            outfilename = calc_tmp.outfilename
            outfilename += ".{}delta".format(i)
            calc_tmp.set_label(outfilename, update_outfilename=True)
            a.set_calculator(calc_tmp)
            self.atoms_list.append(a)

    def set_positions(self, positions, **kwargs):
        pos = self.atoms.get_positions()
        pos[self.index] = positions
        self.atoms.set_positions(pos, **kwargs)
        for atoms in self.atoms_list:
            atoms.set_positions(pos, **kwargs)

    def get_forces(self, *args, **kwargs):
        f = 0.0
        for atoms, o in zip(self.atoms_list, self.fd_operator.operator):
            f += atoms.get_forces(*args, **kwargs)[self.index] * o
        return f

    def get_potential_energy(self, **kwargs):
        e = 0.0
        for atoms, o in zip(self.atoms_list, self.fd_operator.operator):
            e += atoms.get_potential_energy(**kwargs) * o
        return e

    def get_number_of_atoms(self):
        return len(self.atoms)

    def get_velocities(self):
        return self.atoms.get_velocities()

    def get_center_of_mass(self):
        return self.atoms.get_center_of_mass()

    def get_kinetic_energy(self):
        return self.atoms.get_kinetic_energy()

    def get_temperature(self):
        return self.atoms.get_temperature()

    def get_total_energy(self):
        return self.atoms.get_total_energy()

    def get_holu_level(self, **kwargs):
        e = 0.0
        e0 = self.atoms_list[0].get_potential_energy(**kwargs)
        for atoms, o in zip(self.atoms_list, self.fd_operator.operator):
            e += atoms.get_potential_energy(**kwargs) * o
        return -e + e0

    def set_momenta(self, momenta, apply_constraint=True):
        return self.atoms.set_momenta(momenta, apply_constraint=True)

    def get_momenta(self):
        return self.atoms.get_momenta()


class Fd_operator:
    def __init__(self, fd_approx=2, factor=1.0, delta=0.001):

        self.operator = None
        self.fd_approx = fd_approx
        self.factor = factor
        self.delta = delta

        if fd_approx == 2:
            self.operator = [
                1.0 - 3.0 * factor / abs(2.0 * delta),
                4.0 * factor / abs(2.0 * delta),
                -factor / abs(2.0 * delta),
            ]
        elif fd_approx == 1:
            self.operator = [1.0 - factor / abs(delta), factor / abs(delta)]

        else:
            raise IndexError("Please choose fd_approx <= 2")

    def get(self, index):
        if index > len(self.operator):
            raise IndexError("Index exceeded fd_operator length")
        return self.operator[ind]
