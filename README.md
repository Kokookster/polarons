# polarons

A collection of python tools for simulating polarons. A wrapper around ASE. Works currently only for FHI-aims.

Uses a modified potential energy surface to find the minimum configuration of the polaron. If you use this method `Polaron_BFGS`, you should cite:
Sadigh, Babak, Paul Erhart, and Daniel Åberg. "Variational polaron self-interaction-corrected total-energy functional for charge excitations in insulators." Physical Review B 92.7 (2015): 075202.
https://doi.org/10.1103/PhysRevB.92.075202

Please consider to read (and cite) the following paper to learn about finding polarons and sources of errors due to the DFT supercell approach:
Kokott, Sebastian, et al. "First-principles supercell calculations of small polarons with proper account for long-range polarization effects." New Journal of Physics 20.3 (2018): 033023.
https://doi.org/10.1088/1367-2630/aaaf44

## Installation

Requirements: Python3

Clone this repository with:
```
git clone git@gitlab.com:Kokookster/polarons.git
```

You should now have folder named `polarons`. Go into that folder and type:
```
pip install -e .
```

If you now type `pip list | grep polarons` you should see this:
```
polarons                           0.1.0     /cobra/u/seko/software/polarons
```
indicating that `polarons` has been properly installed. You can no use it as any other python package.

## How to start a relaxation

Here is an example script `optimize_polaron.py` for running this kind of relaxation (the script and the whole calculation can be found in the `test/example_run` dir):

```python
from polarons.polaron_BFGS import Polaron_BFGS
from ase.io import read
from ase.calculators.aims import Aims


fmax = 0.001
steps = 100
delta = 0.001
factor = 1.0e0
fd_approx = 2

# Setup the aims calculator
calc = Aims(
    xc="PBE",
    k_grid=(2, 2, 2),
    charge=0.0,
    elsi_restart="read_and_write 100",
    sc_accuracy_forces=1e-4,
    collect_eigenvectors=False,
    aims_command="srun '/u/seko/software/FHIaims/build/O3/aims.200422.scalapack.mpi.x'",
    outfilename="aims.out",
    species_dir="/u/seko/software/FHIaims/species_defaults/light/",
)

# Read the FHI-aims geometry file
c = read("geometry.in")
# Attach calculator
c.set_calculator(calc)

# Generates the Polaron_BFGS optimizer
opt = Polaron_BFGS(
    c, delta=delta, factor=factor, fd_approx=fd_approx, trajectory="geo.traj"
)

with open("steps.dat", "a") as f:
    # Start optimization: Iterate through relaxation steps and writes some information
    # to `steps.dat`.
    for ii, _ in enumerate(opt.irun(fmax=fmax, steps=steps)):
        epot = opt.atoms.get_potential_energy()
        ho = opt.atoms.get_holu_level()
        f.write("{:4d} {:12.5f} {:12.5f}\n".format(ii, ho, epot))
```
Assumes that you have a `geometry.in` file in your current directory. This scripts starts a polaron optimization with light default settings.

Some explanation:
* `fmax`: Maximum force component (for BFGS optimization)
* `steps`: Maximum Number of relaxation steps (for BFGS optimization)
* `delta`: Charge fraction for the finite difference approximation to calculate the homo/lumo force. `delta` > 0 will simulate trapping a hole polaron and `delta` < 0 that of trapping a electron polaron.
* `factor`: Enhancement factor in the finite difference approximation to speed up optimization convergence. At the end this factor SHOULD BE 1.0 for any meaningful result.
* `fd_approx`: Degree of the finite difference approximation to calculate the force of the homo/lumo level. Currently only first (`fd_approx=1`) and second order (`fd_approx=2`) implemented

To run the script just type the following in to the terminal:

`python3 optimize_polaron.py > forces.dat`

The `Polaron_BFGS` optimizer should create `fd_approx + 1` folders, where in each folder a calculation is run with `charge = O*delta`, `charge = 1*delta`,...
The output file for each relaxation step and for each folder are stored (so you can analyze the aims output files if something went wrong).

ASE stores the trajectory of the relaxation in the "geo.traj" file. You can have quick look with the ASE GUI by typing:
```
ase gui geo.traj
```
You should see the 3D visualization.
